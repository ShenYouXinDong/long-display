package com.zx.mes.hyl.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
public class Zxlc extends Model<Zxlc> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    @TableField("LXNum_17")
    private Integer lxnum17;
    @TableField("LXNum_18")
    private Integer lxnum18;
    @TableField("LXNum_191")
    private Integer lxnum191;
    @TableField("LXNum_192")
    private Integer lxnum192;
    private Integer smzs;
    private Float hgl;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLxnum17() {
        return lxnum17;
    }

    public void setLxnum17(Integer lxnum17) {
        this.lxnum17 = lxnum17;
    }

    public Integer getLxnum18() {
        return lxnum18;
    }

    public void setLxnum18(Integer lxnum18) {
        this.lxnum18 = lxnum18;
    }

    public Integer getLxnum191() {
        return lxnum191;
    }

    public void setLxnum191(Integer lxnum191) {
        this.lxnum191 = lxnum191;
    }

    public Integer getLxnum192() {
        return lxnum192;
    }

    public void setLxnum192(Integer lxnum192) {
        this.lxnum192 = lxnum192;
    }

    public Integer getSmzs() {
        return smzs;
    }

    public void setSmzs(Integer smzs) {
        this.smzs = smzs;
    }

    public Float getHgl() {
        return hgl;
    }

    public void setHgl(Float hgl) {
        this.hgl = hgl;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "Zxlc{" +
        "id=" + id +
        ", lxnum17=" + lxnum17 +
        ", lxnum18=" + lxnum18 +
        ", lxnum191=" + lxnum191 +
        ", lxnum192=" + lxnum192 +
        ", smzs=" + smzs +
        ", hgl=" + hgl +
        "}";
    }
}
