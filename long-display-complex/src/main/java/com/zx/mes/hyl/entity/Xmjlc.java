package com.zx.mes.hyl.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
public class Xmjlc extends Model<Xmjlc> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    @TableField("HCNum_4")
    private Integer hcnum4;
    @TableField("WCNum_4")
    private Integer wcnum4;
    @TableField("HCNum_5")
    private Integer hcnum5;
    @TableField("WCNum_5")
    private Integer wcnum5;
    @TableField("HCNum_6")
    private Integer hcnum6;
    @TableField("WCNum_6")
    private Integer wcnum6;
    private Integer smzs;
    private Float hgl;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHcnum4() {
        return hcnum4;
    }

    public void setHcnum4(Integer hcnum4) {
        this.hcnum4 = hcnum4;
    }

    public Integer getWcnum4() {
        return wcnum4;
    }

    public void setWcnum4(Integer wcnum4) {
        this.wcnum4 = wcnum4;
    }

    public Integer getHcnum5() {
        return hcnum5;
    }

    public void setHcnum5(Integer hcnum5) {
        this.hcnum5 = hcnum5;
    }

    public Integer getWcnum5() {
        return wcnum5;
    }

    public void setWcnum5(Integer wcnum5) {
        this.wcnum5 = wcnum5;
    }

    public Integer getHcnum6() {
        return hcnum6;
    }

    public void setHcnum6(Integer hcnum6) {
        this.hcnum6 = hcnum6;
    }

    public Integer getWcnum6() {
        return wcnum6;
    }

    public void setWcnum6(Integer wcnum6) {
        this.wcnum6 = wcnum6;
    }

    public Integer getSmzs() {
        return smzs;
    }

    public void setSmzs(Integer smzs) {
        this.smzs = smzs;
    }

    public Float getHgl() {
        return hgl;
    }

    public void setHgl(Float hgl) {
        this.hgl = hgl;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "Xmjlc{" +
        "id=" + id +
        ", hcnum4=" + hcnum4 +
        ", wcnum4=" + wcnum4 +
        ", hcnum5=" + hcnum5 +
        ", wcnum5=" + wcnum5 +
        ", hcnum6=" + hcnum6 +
        ", wcnum6=" + wcnum6 +
        ", smzs=" + smzs +
        ", hgl=" + hgl +
        "}";
    }
}
