package com.zx.mes.hyl.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.zx.mes.controller.BaseController;
import com.zx.mes.hyl.entity.XRayLC;
import com.zx.mes.hyl.service.XRayLCService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
@RestController
@RequestMapping("/xRayLC")
@CrossOrigin
public class XRayLCController extends BaseController<XRayLCService, XRayLC> {

}

