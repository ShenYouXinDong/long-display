package com.zx.mes.hyl.mapper;

import com.zx.mes.hyl.entity.Ycjlc;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
public interface YcjlcMapper extends BaseMapper<Ycjlc> {

}
