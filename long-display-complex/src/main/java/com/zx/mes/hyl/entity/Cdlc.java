package com.zx.mes.hyl.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
public class Cdlc extends Model<Cdlc> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    @TableField("CDNum_17")
    private Integer cdnum17;
    @TableField("CDNum_18")
    private Integer cdnum18;
    @TableField("CDNum_19")
    private Integer cdnum19;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCdnum17() {
        return cdnum17;
    }

    public void setCdnum17(Integer cdnum17) {
        this.cdnum17 = cdnum17;
    }

    public Integer getCdnum18() {
        return cdnum18;
    }

    public void setCdnum18(Integer cdnum18) {
        this.cdnum18 = cdnum18;
    }

    public Integer getCdnum19() {
        return cdnum19;
    }

    public void setCdnum19(Integer cdnum19) {
        this.cdnum19 = cdnum19;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "Cdlc{" +
        "id=" + id +
        ", cdnum17=" + cdnum17 +
        ", cdnum18=" + cdnum18 +
        ", cdnum19=" + cdnum19 +
        "}";
    }
}
