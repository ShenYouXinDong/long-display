package com.zx.mes.hyl.service.impl;

import com.zx.mes.hyl.entity.Zxlc;
import com.zx.mes.hyl.mapper.ZxlcMapper;
import com.zx.mes.hyl.service.ZxlcService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
@Service
public class ZxlcServiceImpl extends ServiceImpl<ZxlcMapper, Zxlc> implements ZxlcService {

}
