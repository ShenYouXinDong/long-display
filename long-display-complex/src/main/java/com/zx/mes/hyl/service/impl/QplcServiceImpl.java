package com.zx.mes.hyl.service.impl;

import com.zx.mes.hyl.entity.Qplc;
import com.zx.mes.hyl.mapper.QplcMapper;
import com.zx.mes.hyl.service.QplcService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
@Service
public class QplcServiceImpl extends ServiceImpl<QplcMapper, Qplc> implements QplcService {

}
