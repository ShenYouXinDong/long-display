package com.zx.mes.hyl.service.impl;

import com.zx.mes.hyl.entity.Cdlc;
import com.zx.mes.hyl.mapper.CdlcMapper;
import com.zx.mes.hyl.service.CdlcService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
@Service
public class CdlcServiceImpl extends ServiceImpl<CdlcMapper, Cdlc> implements CdlcService {

}
