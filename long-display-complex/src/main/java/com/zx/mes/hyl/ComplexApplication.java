package com.zx.mes.hyl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 310智能车间看板
 *
 * @author 华云龙
 * @date 2018-8-26
 */
@SpringBootApplication
@MapperScan("com.zx.mes.hyl.mapper")
public class ComplexApplication {
    public static void main(String[] args) {
        SpringApplication.run(ComplexApplication.class, args);
    }
}
