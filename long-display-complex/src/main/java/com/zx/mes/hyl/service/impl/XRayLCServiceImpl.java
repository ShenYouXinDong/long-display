package com.zx.mes.hyl.service.impl;

import com.zx.mes.hyl.entity.XRayLC;
import com.zx.mes.hyl.mapper.XRayLCMapper;
import com.zx.mes.hyl.service.XRayLCService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
@Service
public class XRayLCServiceImpl extends ServiceImpl<XRayLCMapper, XRayLC> implements XRayLCService {

}
