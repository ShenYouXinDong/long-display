package com.zx.mes.hyl.service.impl;

import com.zx.mes.hyl.entity.Ycjlc;
import com.zx.mes.hyl.mapper.YcjlcMapper;
import com.zx.mes.hyl.service.YcjlcService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
@Service
public class YcjlcServiceImpl extends ServiceImpl<YcjlcMapper, Ycjlc> implements YcjlcService {

}
