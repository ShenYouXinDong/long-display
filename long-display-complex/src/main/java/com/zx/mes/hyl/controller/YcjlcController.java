package com.zx.mes.hyl.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.zx.mes.controller.BaseController;
import com.zx.mes.hyl.entity.Ycjlc;
import com.zx.mes.hyl.service.YcjlcService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
@RestController
@RequestMapping("/ycjlc")
@CrossOrigin
public class YcjlcController extends BaseController<YcjlcService, Ycjlc> {

}

