package com.zx.mes.hyl.service;

import com.zx.mes.hyl.entity.Cdlc;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
public interface CdlcService extends IService<Cdlc> {

}
