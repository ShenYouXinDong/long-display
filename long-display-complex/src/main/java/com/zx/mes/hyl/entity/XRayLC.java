package com.zx.mes.hyl.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
@TableName("XRayLC")
public class XRayLC extends Model<XRayLC> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    @TableField("WCNum")
    private String WCNum;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWCNum() {
        return WCNum;
    }

    public void setWCNum(String WCNum) {
        this.WCNum = WCNum;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "XRayLC{" +
        "id=" + id +
        ", WCNum=" + WCNum +
        "}";
    }
}
