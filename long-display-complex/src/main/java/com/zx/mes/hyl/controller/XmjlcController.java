package com.zx.mes.hyl.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.zx.mes.controller.BaseController;
import com.zx.mes.hyl.entity.Xmjlc;
import com.zx.mes.hyl.service.XmjlcService;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
@RestController
@RequestMapping("/xmjlc")
@CrossOrigin
public class XmjlcController extends BaseController<XmjlcService, Xmjlc> {

}

