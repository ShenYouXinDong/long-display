package com.zx.mes.hyl.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
public class Ycjlc extends Model<Ycjlc> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    @TableField("HCNum_8")
    private Integer hcnum8;
    @TableField("WCNum_8")
    private Integer wcnum8;
    @TableField("HCNum_9")
    private Integer hcnum9;
    @TableField("WCNum_9")
    private Integer wcnum9;
    @TableField("HCNum_10")
    private Integer hcnum10;
    @TableField("WCNum_10")
    private Integer wcnum10;
    @TableField("HCNum_11")
    private Integer hcnum11;
    @TableField("WCNum_11")
    private Integer wcnum11;
    @TableField("HCNum_12")
    private Integer hcnum12;
    @TableField("WCNum_12")
    private Integer wcnum12;
    @TableField("HCNum_13")
    private Integer hcnum13;
    @TableField("WCNum_13")
    private Integer wcnum13;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHcnum8() {
        return hcnum8;
    }

    public void setHcnum8(Integer hcnum8) {
        this.hcnum8 = hcnum8;
    }

    public Integer getWcnum8() {
        return wcnum8;
    }

    public void setWcnum8(Integer wcnum8) {
        this.wcnum8 = wcnum8;
    }

    public Integer getHcnum9() {
        return hcnum9;
    }

    public void setHcnum9(Integer hcnum9) {
        this.hcnum9 = hcnum9;
    }

    public Integer getWcnum9() {
        return wcnum9;
    }

    public void setWcnum9(Integer wcnum9) {
        this.wcnum9 = wcnum9;
    }

    public Integer getHcnum10() {
        return hcnum10;
    }

    public void setHcnum10(Integer hcnum10) {
        this.hcnum10 = hcnum10;
    }

    public Integer getWcnum10() {
        return wcnum10;
    }

    public void setWcnum10(Integer wcnum10) {
        this.wcnum10 = wcnum10;
    }

    public Integer getHcnum11() {
        return hcnum11;
    }

    public void setHcnum11(Integer hcnum11) {
        this.hcnum11 = hcnum11;
    }

    public Integer getWcnum11() {
        return wcnum11;
    }

    public void setWcnum11(Integer wcnum11) {
        this.wcnum11 = wcnum11;
    }

    public Integer getHcnum12() {
        return hcnum12;
    }

    public void setHcnum12(Integer hcnum12) {
        this.hcnum12 = hcnum12;
    }

    public Integer getWcnum12() {
        return wcnum12;
    }

    public void setWcnum12(Integer wcnum12) {
        this.wcnum12 = wcnum12;
    }

    public Integer getHcnum13() {
        return hcnum13;
    }

    public void setHcnum13(Integer hcnum13) {
        this.hcnum13 = hcnum13;
    }

    public Integer getWcnum13() {
        return wcnum13;
    }

    public void setWcnum13(Integer wcnum13) {
        this.wcnum13 = wcnum13;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "Ycjlc{" +
        "id=" + id +
        ", hcnum8=" + hcnum8 +
        ", wcnum8=" + wcnum8 +
        ", hcnum9=" + hcnum9 +
        ", wcnum9=" + wcnum9 +
        ", hcnum10=" + hcnum10 +
        ", wcnum10=" + wcnum10 +
        ", hcnum11=" + hcnum11 +
        ", wcnum11=" + wcnum11 +
        ", hcnum12=" + hcnum12 +
        ", wcnum12=" + wcnum12 +
        ", hcnum13=" + hcnum13 +
        ", wcnum13=" + wcnum13 +
        "}";
    }
}
