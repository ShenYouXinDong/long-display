package com.zx.mes.hyl.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 华云龙
 * @since 2018-08-26
 */
public class Qplc extends Model<Qplc> {

    private static final long serialVersionUID = 1L;

    private Integer id;
    @TableField("WCNum_1")
    private Integer wcnum1;
    @TableField("WCNum_2")
    private Integer wcnum2;
    @TableField("WCNum_3")
    private Integer wcnum3;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWcnum1() {
        return wcnum1;
    }

    public void setWcnum1(Integer wcnum1) {
        this.wcnum1 = wcnum1;
    }

    public Integer getWcnum2() {
        return wcnum2;
    }

    public void setWcnum2(Integer wcnum2) {
        this.wcnum2 = wcnum2;
    }

    public Integer getWcnum3() {
        return wcnum3;
    }

    public void setWcnum3(Integer wcnum3) {
        this.wcnum3 = wcnum3;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "Qplc{" +
        "id=" + id +
        ", wcnum1=" + wcnum1 +
        ", wcnum2=" + wcnum2 +
        ", wcnum3=" + wcnum3 +
        "}";
    }
}
